﻿using LibFromEducation;
using LibFromEducation.Enum;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;

namespace ChatClient
{
  public static class Client
  {
    public static string userName;
    public static TcpClient TcpClient = new TcpClient();
    public static NetworkStream stream;
    // отправка сообщений
    public static void SendMessage()
    {
      Console.WriteLine("Введите сообщение: ");

      while (true)
      {
        string message = Console.ReadLine();
        Message msg = Client.CreateMessage(message: message, from: 0, status: ClientStatus.LogIn);
        byte[] data = Encoding.Unicode.GetBytes(GlobalVars.JsonSerializer.Serialize(msg));
        stream.Write(data, 0, data.Length);
      }
    }
    // получение сообщений
    public static void ReceiveMessage()
    {
      while (true)
      {
        try
        {
          byte[] data = new byte[64]; // буфер для получаемых данных
          StringBuilder builder = new StringBuilder();
          int bytes = 0;
          do
          {
            bytes = stream.Read(data, 0, data.Length);
            builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
          }
          while (stream.DataAvailable);

          //string message = builder.ToString();
          Message msg = GlobalVars.JsonSerializer.Deserialize<Message>(builder.ToString());
          Console.WriteLine(msg.Msg);//вывод сообщения
        }
        catch
        {
          Console.WriteLine("Подключение прервано!"); //соединение было прервано
          Console.ReadLine();
          Disconnect();
        }
      }
    }

    internal static Message CreateMessage(string message, int from, LibFromEducation.Enum.ClientStatus status)
    {
      return new Message(message: message, from: 0, to: 0) { ClientStatus = status};
    }

    internal static bool ConnectToServer()
    {
      string IpPrimary = GlobalVars.INI.GetPrivateString("Connections", "IpPrimary");
      string port = GlobalVars.INI.GetPrivateString("Connections", "PrimaryPort");

      // check string regular
      Regex regex = new Regex(@"[0-9]{1,}.[0-9]{1,}.[0-9]{1,}.[0-9]{1,}");
      Match matches = regex.Match(IpPrimary);
      if (!(matches.Value == IpPrimary))
      {
        Log("Error Host string in config.ini");
        return false;
      }

      regex = new Regex(@"[0-9]{1,5}");
      matches = regex.Match(port);
      if (!(matches.Value == port))
      {
        Log("Error port string in config.ini");
        return false;
      }
      Client.TcpClient.Connect(IPAddress.Parse(IpPrimary), Convert.ToInt32(port)); //подключение клиента
      Client.stream = Client.TcpClient.GetStream(); // получаем поток
      return true;
    }

    private static void Log(string v, int level = 0)
    {
      throw new NotImplementedException();
    }


    public static void Disconnect()
    {
      if (stream != null)
        stream.Close();//отключение потока
      if (TcpClient != null)
        TcpClient.Close();//отключение клиента
      Environment.Exit(0); //завершение процесса
    }
  }
}
