﻿using LibFromEducation;
using LibFromEducation.Enum;
using System;
using System.Text;
using System.Threading;

namespace ChatClient
{
	class Program
	{
		static void Main(string[] args)
		{
			Client.userName = GlobalVars.INI.GetPrivateString("User", "Name");
			if (string.IsNullOrEmpty(Client.userName))
			{
				Console.Write("Введите свое имя: ");
				Client.userName = Console.ReadLine();
				if (!string.IsNullOrEmpty(Client.userName)) GlobalVars.INI.WritePrivateString("User", "Name", Client.userName);
			}

			try
			{
				if (Client.ConnectToServer())
				{
					Message msg = Client.CreateMessage(message: $"{Client.userName} loging", from: 0, status: ClientStatus.LogIn);
					byte[] data = Encoding.Unicode.GetBytes(GlobalVars.JsonSerializer.Serialize(msg));
					Client.stream.Write(data, 0, data.Length);

					// запускаем новый поток для получения данных
					Thread receiveThread = new Thread(new ThreadStart(Client.ReceiveMessage));
					receiveThread.Start(); //старт потока
					Console.WriteLine("Добро пожаловать, {0}", Client.userName);
					Client.SendMessage();
				}
				else
				{
					Console.WriteLine("Ошибка подключения к серверу");
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				Client.Disconnect();
			}
		}
	}
}
